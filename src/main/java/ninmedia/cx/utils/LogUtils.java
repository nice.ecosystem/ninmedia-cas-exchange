package ninmedia.cx.utils;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class LogUtils {
    @Autowired
    NamedParameterJdbcTemplate jdt;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    
    public  void addLog(String reqRef, String reqFromIp, String reqFromHost) {
        log.info("Adding to log "+reqRef+" from: "+reqFromIp+", Host: "+reqFromHost);
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("reqRef", reqRef);
        sqlParams.addValue("reqFromIp", reqFromIp);
        sqlParams.addValue("reqFromHost", reqFromHost);

        String sql = " INSERT INTO log_req (req_ref,req_from_ip, req_from_host) VALUES (:reqRef,:reqFromIp,:reqFromHost) ";
        jdt.update(sql, sqlParams);
    }

    public void updateLogResponse(String reqRef, JSONObject jsonResult, HttpStatus httpStatus, Exception e) {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("reqRef", reqRef);
        sqlParams.addValue("reqErrorCode", jsonResult.getJSONObject("Header").getInt("RespCode"));
        sqlParams.addValue("reqErrorDesc", jsonResult.getJSONObject("Header").getString("RespDescription"));
        sqlParams.addValue("reqHttpStatus", httpStatus.value());
        sqlParams.addValue("exceptionMsg", (e!=null)?e.getMessage():null);

        String sql = " UPDATE log_req SET resp_error_code=:reqErrorCode, resp_error_desc=:reqErrorDesc, resp_http_status=:reqHttpStatus,resp_exception_msg=:exceptionMsg WHERE req_ref=:reqRef ";
        jdt.update(sql, sqlParams);
    }

    public void updateReqParameters(String reqRef, JSONObject jsonReq) {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("reqRef", reqRef);
        sqlParams.addValue("token", jsonReq.getJSONObject("Header").getString("Token"));
        sqlParams.addValue("method", jsonReq.getJSONObject("Header").getString("Method"));
        sqlParams.addValue("reqDateUtc", jsonReq.getJSONObject("Header").getString("ReqDateTimeUTC"));
        sqlParams.addValue("reqUserRef", jsonReq.getJSONObject("Header").getString("ReqUserRef"));

        String sql = " UPDATE log_req SET req_token=:token, req_date_utc=:reqDateUtc, req_method=:method,req_user_ref=:reqUserRef WHERE req_ref=:reqRef ";
        jdt.update(sql, sqlParams);
    }

    public void updateReqUser(String reqRef, JSONObject jsonReq) {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("reqRef", reqRef);
        sqlParams.addValue("userId", jsonReq.getJSONObject("User").getInt("user_id"));
        String sql = " UPDATE log_req SET req_user_id=:userId WHERE req_ref=:reqRef ";
        jdt.update(sql, sqlParams);
    }
}