package ninmedia.cx.utils;

import java.util.HashMap;
import java.util.Map;

public class MethodClassMap {
	private final static Map<String,String> map = new HashMap<String,String>();
	
	public static Map<String,String> getMap() {
		if (map.size() == 0) {
			map.put("open.account", "OpenAccount");
			map.put("close.account", "CloseAccount");
			map.put("send.osd", "SendOsd");
			map.put("activate.product", "AuthProduct");
			map.put("deactivate.product", "UnAuthProduct");
			map.put("sub.add", "SubAdd");
			map.put("sub.refresh", "SubRefresh");
			map.put("get.stb", "GetStb");
			
		}
		return map;
	}
}
