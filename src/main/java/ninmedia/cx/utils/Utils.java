package ninmedia.cx.utils;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public class Utils {

	private static NamedParameterJdbcTemplate jdbcTemplateSql;
	private static NamedParameterJdbcTemplate jdbcTemplateMy;
	private static Properties prop = null;
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private static Principal principal;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static NamedParameterJdbcTemplate getJdbcTemplateSql() {
		return jdbcTemplateSql;
	}
	public static void setJdbcTemplateSql(NamedParameterJdbcTemplate jdbcTemplateSql) {
		Utils.jdbcTemplateSql = jdbcTemplateSql;
	}
	
	public static void setPrincipal(Principal principalx) {
		principal = principalx;		
	}
	
	public static Principal getPrincipal() {
		return principal;
	}
	
	public static String getProperty(String key) {
		try {
			if (prop == null) {																							
				InputStream in = Utils.class.getClassLoader().getResourceAsStream("config.properties");
				prop = new Properties();																						
				prop.load(in);
			}			
			String value = prop.getProperty(key);
			return value;
		} catch (Exception e) {
			log.error("Error loading properties file: "+e,e);
			return null;
		}					
	}
	
	public static String getConfig(String key) {
		try {
			MapSqlParameterSource sqlParams = new MapSqlParameterSource();
			sqlParams.addValue("key", key);
			String sql = " SELECT config_value FROM config WHERE config_key=:key ";
			String value = jdbcTemplateMy.queryForObject(sql, sqlParams, String.class);
			return value;			
		} catch (Exception e) {
			log.error("Config with key: "+key+", not found!");
			return null;
		}
	}
	
	public static JSONObject generateJsonError(int responseCode, String errorDescription, JSONObject jsonRequest) {						
		JSONObject jsonResult = new JSONObject();		
		JSONObject header = new JSONObject();
		JSONObject data = new JSONObject();				
				
		header.put("Success", false);
		header.put("RespCode", responseCode);
		header.put("RespDescription", errorDescription);
		header.put("SysRef", jsonRequest.getJSONObject("Header").getString("SysRef"));
		
		if (jsonRequest.has("ReqUserRef") && jsonRequest.has("Method")) {
			header.put("ReqUserRef", jsonRequest.getJSONObject("Header").getString("ReqUserRef"));			
			header.put("Method", jsonRequest.getJSONObject("Header").getString("Method"));
		}		
		
		jsonResult.put("Header", header);
		jsonResult.put("Data", data);
		
		return jsonResult;		
	}
	
	public static JSONObject generateJsonSuccess(JSONObject jsonRequest) {		
		
		JSONObject jsonResult = new JSONObject();		
		JSONObject header = jsonRequest.getJSONObject("Header");
		JSONObject data = jsonRequest.getJSONObject("Data");
		
		header.put("Success", true);
		header.put("RespCode", 0);
		header.put("RespDescription", "Succes");
		header.put("RespDateTimeUTC", getServerDateTimeUTC());		
		
		jsonResult.put("Header", header);
		jsonResult.put("Data", data);
		
		return jsonResult;
	}
		
	
	public static String getServerDateTimeUTC() {
		try {
			MapSqlParameterSource sqlParams = new MapSqlParameterSource();			
			String sql = " SELECT UTC_TIMESTAMP() ";
			Date value = jdbcTemplateMy.queryForObject(sql, sqlParams, Date.class);
			return dateFormat.format(value);			
		} catch (Exception e) {
			log.error("Error retrieving server date/time UTC! "+e,e);
			return null;
		}
	}
	
	public static void validateCardId(JSONObject jsonRequest) throws Exception {
		Map<String,Object> rs = null;
		int stbId = 0;
		if (!jsonRequest.getJSONObject("Data").has("CardId")) {
			if (!jsonRequest.getJSONObject("Data").has("QR")) throw new Exception("QR is not supplied!");			
			MapSqlParameterSource sqlParams = new MapSqlParameterSource();
			sqlParams.addValue("QR", jsonRequest.getJSONObject("Data").getString("QR"));
			
			String sql = " SELECT s.*, c.cas_name, c.cas_alias FROM stb s INNER JOIN cas c ON (s.cas_id=c.cas_id) WHERE s.qr=:QR ";
			rs = jdbcTemplateMy.queryForMap(sql, sqlParams);
			String cardId = rs.get("chip_id").toString();
			stbId = (int) ((long) rs.get("stb_id"));
			jsonRequest.getJSONObject("Data").put("CardId", cardId);					
		} else {			
			if (!jsonRequest.getJSONObject("Data").has("CardId")) throw new Exception("CardId is not supplied!");
			final long cardId = jsonRequest.getJSONObject("Data").getLong("CardId");			
			log.info("Validating using Card ID instead of QR: "+cardId);
			MapSqlParameterSource sqlParams = new MapSqlParameterSource();
			sqlParams.addValue("CardId", cardId);
			
			String sql = " SELECT s.*, c.cas_name, c.cas_alias FROM stb s INNER JOIN cas c ON (s.cas_id=c.cas_id) WHERE s.chip_id=:CardId ";
			rs = jdbcTemplateMy.queryForMap(sql, sqlParams);
			String QR = rs.get("qr").toString();
			stbId = (int) ((long) rs.get("stb_id"));
			
			jsonRequest.getJSONObject("Data").put("QR", QR);
					
		}
		
		jsonRequest.getJSONObject("Data").put("CasName", rs.get("cas_alias"));
		jsonRequest.getJSONObject("Data").put("StbId", stbId);	
	}
	
	public static boolean cardIsActive(String cardId) throws Exception {
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("CardId", cardId);
		
		String sql = " SELECT count(s.stb_id) FROM stb s INNER JOIN log_stb a ON (s.stb_id=a.stb_id) WHERE s.chip_id=:CardId ";
		int count = jdbcTemplateMy.queryForObject(sql, sqlParams, Integer.class);
		
		if (count > 0) return true;
		
		return false;
	}
	
	public static void addNewActiveCard(String cardId, int userId) {
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("CardId", cardId);
		sqlParams.addValue("UserId", userId);
		
	}
	

	
	public static String byteArrayToOctet(byte[] data) {
		String result = "";
		for(byte b : data) {
			result+= String.format("%02X", b);
		}
		return result;
	}
	
}
