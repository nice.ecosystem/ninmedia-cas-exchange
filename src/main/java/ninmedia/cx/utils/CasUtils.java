package ninmedia.cx.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CasUtils {
    @Autowired
    NamedParameterJdbcTemplate jdt;

    public Map<String,Object> getCasDetail(int casId) throws Exception {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("casId", casId);
        String sql = " SELECT * FROM cas WHERE cas_id=:casId ";
        Map<String,Object> casDetail = jdt.queryForMap(sql, sqlParams);
        return casDetail;
    }
}
