package ninmedia.cx.utils;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class AuthUtils {
	@Autowired
	NamedParameterJdbcTemplate jdt;

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
		
	public Map<String,Object> auth(String token, String reqDateTime, String reqIp) throws Exception {
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("reqDateTime", reqDateTime);
		sqlParams.addValue("token", token);
		sqlParams.addValue("reqIp", reqIp);
		sqlParams.addValue("validity", Integer.parseInt(Utils.getConfig("auth.validity.minute")));
		
		String sql = " SELECT u.user_id,u.username,u.name FROM user u INNER JOIN user_allow_host h ON (u.user_id=h.user_id)  ";
		sql+= " WHERE SHA2(CONCAT(username,password,:reqDateTime),256)=:token AND (ip=:reqIp OR ip='*') ";
		sql+= " AND :reqDateTime >= SUBDATE(UTC_TIMESTAMP(), INTERVAL :validity MINUTE) AND :reqDateTime < ADDDATE(UTC_TIMESTAMP(), INTERVAL :validity MINUTE) ";
		
		Map<String,Object> queryResult = jdt.queryForMap(sql, sqlParams);
		log.info("Authentication successfull");
		return queryResult;
	}
}
