package ninmedia.cx.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import ninmedia.cx.core.SumaQueueResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SocketUtil {

	@Autowired
	CasUtils casUtils;

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private final static Map<Integer,Socket> socketPool = new HashMap<>();
	private final static Map<Integer,Timer> socketTimerPool = new HashMap<>();
	
	public Socket getSocket(int casId) throws Exception {
		Timer timer = null;
		
		Map<String,Object> casDetail = casUtils.getCasDetail(casId);
		final String casHost = casDetail.get("cas_ip").toString();
		final int casPort = Math.toIntExact((long) casDetail.get("cas_port"));
		
		if (socketTimerPool.get(casId)!=null) {
			socketTimerPool.get(casId).cancel();	
			timer = socketTimerPool.get(casId);
			timer = null;			
			socketTimerPool.put(casId, timer);
		}			
		
		if (socketPool.get(casId) == null || socketPool.get(casId).isClosed()) {			
			log.info("Creating new socket to: "+casHost+", port: "+casPort);
			Socket socket = new Socket();
			socket.setSoTimeout(10000);
	    	socket.connect(new InetSocketAddress(casHost, casPort), 10000);
	    	log.info("Socket to "+casHost+" is connected!");
	    	socketPool.put(casId, socket);
		}					
		
		try {			
			socketTimerPool.put(casId, new Timer());
			socketTimerPool.get(casId).schedule(new TimerTask() {			
				@Override
				public void run() {				
					try {
						if (socketPool.get(casId).isClosed()) return;
						log.info("Socket CAS : "+casId+" is idle, closing socket connection!");
						socketPool.get(casId).close();	
						log.info("Socket CAS : "+casId+" closed!");
					} catch (IOException e) {
						log.error("Error while closing socket!");
					}
				}
			}, Long.parseLong(Utils.getConfig("suma.socket.ttl")));
		} catch (IllegalStateException e) {
			log.error("Error while creating new timer! "+e);
		}
				
		return socketPool.get(casId);
	}
	
	public Socket createSocket(int casId) throws Exception  {
		
		try {
			Map<String,Object> casDetail = casUtils.getCasDetail(casId);
			
			final String casHost = casDetail.get("cas_ip").toString();
			final int casPort = Math.toIntExact((long) casDetail.get("cas_port"));
			
			log.info("Creating new socket to: "+casHost+", port: "+casPort);
			
			Socket socket = new Socket();
			socket.setSoTimeout(10000);
	    	socket.connect(new InetSocketAddress(casHost, casPort), 10000);
	    	
	    	log.info("Socket to "+casHost+" is connected!");
	    	
	    	return socket;
		} catch (Exception e) {
			log.error("Error creating socket: "+e,e);
			throw e;
		}					
	}
	
	public SumaQueueResponse sendRequest(int casId, byte[] packet) throws IOException, Exception {
		Socket socket = createSocket(casId);
		OutputStream output = socket.getOutputStream();	
		output.write(packet);
		
		InputStream input = socket.getInputStream();
		byte[] header = new byte[6];
		
		input.read(header);				
		log.info("Response header data: "+getByteString(header));
		
		ByteBuffer byteBuffer = ByteBuffer.wrap(header);
		
		short respSessionId = byteBuffer.getShort();
		byte respCasVersion = byteBuffer.get();
		byte respCommandId = byteBuffer.get();
		short respBodyLength = byteBuffer.getShort();
		
		log.info("Body length: "+respBodyLength);
		
		byte[] body = new byte[respBodyLength];
		input.read(body);	
		log.info("Response body data: "+getByteString(body));			
		byteBuffer = ByteBuffer.wrap(body);
		
		try {
			socket.close();
			log.info("Socket is closed!");
		} catch (Exception e) {
			log.error("Error closing socket: "+e);
		}
		
		return new SumaQueueResponse(header, body);
	}
	
	private static String getByteString(byte[] packet) {
		String result = "";
		for (int i=0;i<packet.length;i++) {
			result+=String.format("%02X ", packet[i]);			
		}
		return result.trim();
	}
}
