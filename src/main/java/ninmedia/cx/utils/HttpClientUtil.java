package ninmedia.cx.utils;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientUtil {
	
	private final static MediaType MEDIA_TYPE = MediaType.parse("text/plain");
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            builder.readTimeout(30000, TimeUnit.MILLISECONDS);
            builder.connectTimeout(30000, TimeUnit.MILLISECONDS);
            builder.retryOnConnectionFailure(false);
            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public static void post(String URL, String text) throws IOException {		
		OkHttpClient httpClient = HttpClientUtil.getUnsafeOkHttpClient();
		RequestBody body = RequestBody.create(MEDIA_TYPE, text);
		final Request request = new Request.Builder()
			       .url(URL)
			       .post(body)
			       .addHeader("Content-Type", "text/plain")			       
			       .addHeader("cache-control", "no-cache")
			       .build();
		Response response = httpClient.newCall(request).execute();
		String mMessage = response.body().string();
		log.info("Result string: "+mMessage);
		try {
			//log.info(Util.formatXml(mMessage, false));
		} catch (Exception e) {}									
	}
}
