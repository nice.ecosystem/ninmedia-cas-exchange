package ninmedia.cx.controller;

import ninmedia.cx.api.ApiResponse;
import ninmedia.cx.utils.*;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

@RestController
public class Default {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	@Autowired
	private ApplicationContext context;
	@Autowired
	NamedParameterJdbcTemplate jdt;
	@Autowired
	private LogUtils logUtils;
	@Autowired
	private AuthUtils authUtils;

	@RequestMapping(value = {"/api/xml2"},produces = "application/xml;charset=UTF-8")
	public DeferredResult<ResponseEntity<String>> xmlApi(HttpServletRequest request) {
				
		DeferredResult<ResponseEntity<String>> output = new DeferredResult<>();
		
		ForkJoinPool.commonPool().submit(()-> {
			ResponseEntity<String> result = apiHandler(request);
			output.setResult(result);        
		});
		
		return output;						
	}
	
	@RequestMapping(value = {"/xml"},produces = "application/xml;charset=UTF-8")
	private ResponseEntity<String> apiHandler(HttpServletRequest request) {
		final String sysRef = UUID.randomUUID().toString();	
		JSONObject jsonResult = new JSONObject();		
		JSONObject jsonReq = new JSONObject();
		jsonReq.put("Header",(new JSONObject()).put("SysRef", sysRef));

		log.debug("Log utils entity: "+logUtils);
		log.debug("Auth utils entity: "+authUtils);


		logUtils.addLog(sysRef, request.getRemoteAddr(), request.getRemoteHost());
	
		if (!request.getMethod().toLowerCase().equals("post")) {			
			return generateXmlResult(Utils.generateJsonError(1011, "Invalid method type, please use POST instead", jsonReq), HttpStatus.BAD_REQUEST, null);
		}
		
		String xmlRequest = null;		
		String reqDateTime = null;
		String token = null;
		String reqIp = request.getRemoteAddr();
		String method = null;		
		
		// validate xml
		try {
			xmlRequest = IOUtils.toString(request.getReader());			
			XmlUtils.validate(xmlRequest, "common.request.xsd");
			
			log.info("XML Request: "+xmlRequest);			
			
			jsonReq = XML.toJSONObject(xmlRequest).getJSONObject("Request");
			log.info("JSON Request: "+jsonReq.toString(4));
			
			jsonReq.getJSONObject("Header").put("SysRef", sysRef);
			reqDateTime = jsonReq.getJSONObject("Header").getString("ReqDateTimeUTC");
			token = jsonReq.getJSONObject("Header").getString("Token");
			method = jsonReq.getJSONObject("Header").getString("Method");
			
			XmlUtils.validate(xmlRequest, method+".xsd");
			
			logUtils.updateReqParameters(sysRef, jsonReq);			
			
			log.info("Req. Token: "+token);
			log.info("Req. Date/Time (UTC): "+reqDateTime);
			log.info("Req. From: "+reqIp);
		} catch (Exception e) {
			log.info("Invalid XML parameters: "+e);
			jsonResult.put("Success", false);
			jsonResult.put("ErrorDescription", "Invalid XML request parameters");
			return generateXmlResult(Utils.generateJsonError(1013, "Invalid XML request parameters", jsonReq), HttpStatus.BAD_REQUEST, e);			
		}	
		
		// auth
		try {
			Map<String,Object> user = authUtils.auth(token, reqDateTime, reqIp);
			jsonReq.put("User", user);
			logUtils.updateReqUser(sysRef, jsonReq);
		} catch (Exception e) {
			log.error("Authentication failed!");			
			return generateXmlResult(Utils.generateJsonError(1015, "Forbidden/Invalid authentication", jsonReq), HttpStatus.FORBIDDEN, e);
		}
		
		try {
			ApiResponse apiResponse = processRequest(jsonReq, request);
			return generateXmlResult(apiResponse.getJsonResult(), apiResponse.getHttpStatus(), null);
		} catch (Exception e) {			
			ApiResponse apiResponse = ApiResponse.createInstance(Utils.generateJsonError(1023, "Unknown error: "+e, jsonReq), HttpStatus.INTERNAL_SERVER_ERROR);			
			log.error("Unknown error ["+sysRef+"]: "+e,e);
			return generateXmlResult(apiResponse.getJsonResult(), apiResponse.getHttpStatus(),e);
		}
	}


	
	@Async
	public Future<String> call() throws InterruptedException  {
		Thread.sleep(3000);
		return new AsyncResult<String>("return value");
	}
	
	
	private ResponseEntity<String> generateXmlResult(JSONObject jsonResult, HttpStatus httpStatus, Exception e) {
		String resultStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+XML.toString(jsonResult, "Response"); 
		String sysRef = jsonResult.getJSONObject("Header").getString("SysRef");
		
		logUtils.updateLogResponse(sysRef, jsonResult, httpStatus, e);
		
		log.info("XML Response: "+resultStr);
		
		return new ResponseEntity<String>(resultStr, httpStatus);
	}	

	
	private ApiResponse processRequest(JSONObject jsonRequest, HttpServletRequest request) {
		String sysRef = jsonRequest.getJSONObject("Header").getString("SysRef");
		String className = MethodClassMap.getMap().get(jsonRequest.getJSONObject("Header").getString("Method"));
		String methodName = "getResponse";
		
		try {
			Class<?> c =  Class.forName("ninmedia.cx.api."+className);	
			Method initMethod = c.getMethod("init", String.class, JSONObject.class, DataSource.class, ServletContext.class, HttpServletRequest.class, NamedParameterJdbcTemplate.class);
			Method processMethod = c.getMethod(methodName);
	        Object obj = c.newInstance();
	        initMethod.invoke(obj, sysRef, jsonRequest, context, request, jdt);
	        ApiResponse apiResponse = (ApiResponse) processMethod.invoke(obj);
	        return apiResponse;
		} catch (ClassNotFoundException e) {
			log.info("Method : "+className+" is not found!");
			JSONObject jsonResult = Utils.generateJsonError(1021, "Invalid method name", jsonRequest);			
			return ApiResponse.createInstance(jsonResult, HttpStatus.SERVICE_UNAVAILABLE);
		} catch (Exception e) {
			log.info("Unknown error: "+e,e);
			JSONObject jsonResult = Utils.generateJsonError(1099, "Unknown error", jsonRequest);
			return ApiResponse.createInstance(jsonResult, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	
	
}
