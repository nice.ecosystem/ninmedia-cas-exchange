package ninmedia.cx.api;

import org.springframework.http.HttpStatus;

import ninmedia.cx.utils.Utils;

public class GetStb extends AbstractApi {

	@Override
	public ApiResponse getResponse() throws Exception {
		// TODO Auto-generated method stub
		try {
			Utils.validateCardId(jsonRequest);
		} catch (Exception e) {
			log.info("Failed validate Card Id: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2102, "Invalid Card ID or QR!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		
		return null;
	}

}
