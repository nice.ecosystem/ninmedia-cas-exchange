package ninmedia.cx.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import ninmedia.cx.utils.Utils;

public class SubAdd extends AbstractApi {
	
	Map<String,Object> subProductDetail = null;
	public static final int SUB_PRODUCT_TYPE_DURATION = 1;
	public static final int SUB_PRODUCT_TYPE_FIXDATE = 2;
	long reqId = 0;
	private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Map<String,Object> productDetail = null;
	String cardId = null;
	
	@Override
	public ApiResponse getResponse() throws Exception {	
		try {
			Utils.validateCardId(jsonRequest);
		} catch (Exception e) {
			log.info("Failed validate Card Id: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2102, "Invalid Card ID or QR!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
				
		try {
			getCasDetail();
		} catch (Exception e) {
			log.info("Failed when getting CAS detail: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2101, "Couldn't find detail of CAS: "+casName, jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		try {
			getSubProductDetail();
		} catch (Exception e) {
			log.info("Failed when getting sub product detail: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2301, "Invalid product "+jsonRequestData.getString("ProductName"), jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		
		try {
			productDetail = getProductDetail(subProductDetail.get("product_alias").toString());
		} catch (Exception e) {
			log.info("Failed validate Product Id: "+e,e);
			return ApiResponse.createInstance(Utils.generateJsonError(2103, "Invalid product!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
						
		cardId = jsonRequestData.getString("CardId");		
		log.info("Open account: "+cardId);
		
		int productType = (int) subProductDetail.get("type_id");
		
		ApiResponse apiResp = null;
		
		try {						
			switch (productType) {
				case SUB_PRODUCT_TYPE_DURATION: apiResp = processProductDuration(); break;
				case SUB_PRODUCT_TYPE_FIXDATE: apiResp = processProductFixDate(); break;
			}
		} catch (Exception e) {
			log.info("Error while subscribe product: "+e,e);
			return ApiResponse.createInstance(Utils.generateJsonError(2302, "Error while subscribe product", jsonRequest), HttpStatus.BAD_REQUEST);
		}				
		
		return apiResp;
	}
	
	private void getSubProductDetail() throws Exception {
		String productName = jsonRequestData.getString("ProductName").toUpperCase().trim();
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("ProductName", productName);
		String sql = " SELECT * FROM sub_product sp INNER JOIN cas_product cp ON (cp.product_id=sp.cas_product_id) WHERE sp.alias=:ProductName ";
		subProductDetail = jdt.queryForMap(sql, sqlParams);
		log.info("Sub Product Detail: "+subProductDetail);						
	}
	
	private ApiResponse processProductDuration() throws Exception {
		long validDuration = (long) subProductDetail.get("valid_duration");
		Calendar cal1 = Calendar.getInstance();
		Date beginTime = cal1.getTime();		
		Date endTime = null;
				
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("casProductId", subProductDetail.get("cas_product_id"));
		paramMap.addValue("stbId", jsonRequest.getJSONObject("Data").get("StbId"));		
		
		String sql = " SELECT * FROM sub_req WHERE stb_id=:stbId AND cas_product_id=:casProductId AND is_active=1 ORDER BY end_time DESC LIMIT 0,1 ";
		List<Map<String,Object>> qr = jdt.queryForList(sql, paramMap);
		for(Map<String,Object> rec : qr) {
			//paramMap.addValue("endTime", rec.get("end_time"));
			Timestamp ts1 = (Timestamp) rec.get("begin_time");
			Timestamp ts2 = (Timestamp) rec.get("end_time");
			
			beginTime = new Date(ts1.getTime());				
			cal1.setTime(new Date(ts2.getTime()));
		}
		
		cal1.add(Calendar.DATE, (int) validDuration);
		endTime = cal1.getTime();
		
		log.info("Begin time: "+df.format(beginTime)+" - "+df.format(endTime));
		
		ApiResponse apiResponse = sendRequest(beginTime, endTime);
		return apiResponse;
	}
	
	private ApiResponse processProductFixDate() throws Exception {
		log.info("Process product with fix date type");
		Timestamp ts = (Timestamp) subProductDetail.get("valid_datetime");
		
		Date beginTime = new Date();
		Date endTime = new Date(ts.getTime());
		
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("casProductId", subProductDetail.get("cas_product_id"));
		paramMap.addValue("stbId", jsonRequest.getJSONObject("Data").get("StbId"));
		paramMap.addValue("endTime", endTime);
		
		String sql = " SELECT * FROM sub_req WHERE stb_id=:stbId AND cas_product_id=:casProductId AND is_active=1 AND end_time>:endTime ";
		List<Map<String,Object>> qr = jdt.queryForList(sql, paramMap);
		for(Map<String,Object> rec : qr) {
			//paramMap.addValue("endTime", rec.get("end_time"));
			ts = (Timestamp) rec.get("end_time");
			endTime = new Date(ts.getTime());			
		}
		
		ApiResponse apiResponse = sendRequest(beginTime, endTime);
		return apiResponse;
	}
	
	private ApiResponse sendRequest(Date beginTime, Date endTime) throws Exception {
		log.info("Add sub request: "+df.format(beginTime)+" until "+df.format(endTime));
						
		KeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("reqRef", reqRef);
		paramMap.addValue("subProductId", subProductDetail.get("sub_product_id"));
		paramMap.addValue("casProductId", subProductDetail.get("cas_product_id"));
		paramMap.addValue("stbId", jsonRequest.getJSONObject("Data").get("StbId"));
		paramMap.addValue("userId", jsonRequesUser.get("user_id"));
		paramMap.addValue("beginTime", beginTime);
		paramMap.addValue("endTime", endTime);
								
		int beginTimeS = (int) (beginTime.getTime()/1000L);
		int endTimeS = (int) (endTime.getTime()/1000L);
		
		String sql = " INSERT INTO sub_req (req_ref,sub_product_id,cas_product_id,stb_id,user_id,begin_time,end_time) VALUES (:reqRef,:subProductId,:casProductId,:stbId,:userId,:beginTime,:endTime) ";
		jdt.update(sql, paramMap, holder);
		long subId = holder.getKey().longValue();
		
		paramMap.addValue("subId", subId);
		
		ApiResponse apiResp = casApi.authProduct(cardId, productDetail, beginTimeS, endTimeS);
		//apiResp.getJsonResult().getJSONObject("Data").put("BeginTime", df.format(beginTime));
		apiResp.getJsonResult().getJSONObject("Data").put("EndTime", df.format(endTime));
		apiResp.getJsonResult().getJSONObject("Data").put("SubId", subId);
		
		if (apiResp.getJsonResult().getJSONObject("Header").getBoolean("Success")) {						
			sql = " UPDATE sub_req SET success=1, is_active=1 WHERE sub_id=:subId ";
			jdt.update(sql, paramMap);
			
			sql = " UPDATE sub_req SET is_active=0 WHERE stb_id=:stbId AND cas_product_id=:casProductId AND sub_id!=:subId AND is_active=1 ";
			jdt.update(sql, paramMap);
		}
		
		return apiResp;
	}		
	
}
