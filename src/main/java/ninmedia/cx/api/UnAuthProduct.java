package ninmedia.cx.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.http.HttpStatus;

import ninmedia.cx.utils.Utils;

public class UnAuthProduct extends AbstractApi {

	@Override
	public ApiResponse getResponse() throws Exception {
		try {
			Utils.validateCardId(jsonRequest);
		} catch (Exception e) {
			log.info("Failed validate Card Id: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2102, "Invalid Card ID or QR!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		try {
			getCasDetail();
		} catch (Exception e) {
			log.info("Failed when getting CAS detail: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2101, "Couldn't find detail of CAS: "+casName, jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		String cardId = ""+jsonRequestData.getLong("CardId");
		
		Map<String,Object> productDetail = null;
		try {
			productDetail = getProductDetail(jsonRequestData.getString("ProductId"));
		} catch (Exception e) {
			log.info("Failed validate Product Id: "+e,e);
			return ApiResponse.createInstance(Utils.generateJsonError(2103, "Invalid product!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		Date beginTime = null;
		Date endTime = null;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			beginTime = df.parse("2010-01-02 00:00:00");
			endTime = df.parse("2010-01-01 00:00:00");
			log.info("Begin time: "+beginTime+", end time: "+endTime);
		} catch (Exception e) {
			log.info("Failed validate date begin / end time: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2104, "Invalid begin or end date/time!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		int beginTimeS = (int) (beginTime.getTime()/1000L);
		int endTimeS = (int) (endTime.getTime()/1000L);
								
		ApiResponse apiResp = casApi.authProduct(cardId, productDetail, beginTimeS, endTimeS);	
		
		addActivationLog(productDetail, beginTime, endTime,apiResp.getJsonResult().getJSONObject("Header").getBoolean("Success"));
		
		return apiResp;
	}	
}
