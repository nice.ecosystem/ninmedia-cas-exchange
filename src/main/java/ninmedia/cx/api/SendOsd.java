package ninmedia.cx.api;

import org.springframework.http.HttpStatus;

import ninmedia.cx.utils.Utils;

public class SendOsd extends AbstractApi {
	@Override
	public ApiResponse getResponse() throws Exception {
		try {
			Utils.validateCardId(jsonRequest);
		} catch (Exception e) {
			log.info("Failed validate Card Id: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2102, "Invalid Card ID or QR!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		try {
			getCasDetail();
		} catch (Exception e) {
			log.info("Failed when getting CAS detail: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2101, "Couldn't find detail of CAS: "+casName, jsonRequest), HttpStatus.BAD_REQUEST);
		}		
				
		String cardId = ""+jsonRequestData.getLong("CardId");
		String message = ""+jsonRequestData.get("Message");
				
		log.info("Send OSD message to : "+cardId+", message: "+message);
		
		// check if box is active or not ...
		
		return casApi.sendOsd(cardId, message);
	}
}
