package ninmedia.cx.api;

import java.lang.invoke.MethodHandles;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import ninmedia.cx.core.CasApi;
import ninmedia.cx.core.SumaApi;

public abstract class AbstractApi {
	protected ApplicationContext context;
	protected HttpServletRequest request;	
	protected NamedParameterJdbcTemplate jdbcTemplate;
	protected DataSource dataSource;
	protected JSONObject jsonRequest;
	protected JSONObject jsonRequestData;
	protected JSONObject jsonRequestHeader;
	protected JSONObject jsonRequesUser;
	protected TransactionTemplate transactionTemplate;
	protected String apiType = null;
	protected String reqRef = null;
	protected NamedParameterJdbcTemplate jdt;
	
	
	//
	Map<String,Object> casDetail = null;
	String casName = null;
	protected CasApi casApi = null;
	static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public AbstractApi() {

	}
	
	public void init(String reqRef, JSONObject jsonRequest, ApplicationContext context, HttpServletRequest request, NamedParameterJdbcTemplate jdbcTemplate) {
		jdt = jdbcTemplate;
		this.dataSource = dataSource;
		this.context = context;
		this.request = request;
		this.reqRef = reqRef;
		this.jdbcTemplate = jdbcTemplate;
		this.jsonRequest = jsonRequest;
		this.jsonRequestData = jsonRequest.getJSONObject("Data");
		this.jsonRequestHeader = jsonRequest.getJSONObject("Header");
		jsonRequesUser = jsonRequest.getJSONObject("User");
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
		transactionTemplate = new TransactionTemplate(transactionManager);
	};
	
	public abstract ApiResponse getResponse() throws Exception;
	
	protected void getCasDetail() throws Exception {
		casName = jsonRequestData.getString("CasName");
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("casName", casName);
		String sql = " SELECT * FROM cas WHERE cas_alias=:casName ";
		casDetail = jdt.queryForMap(sql, sqlParams);
		log.info("CAS detail: "+casDetail);
		apiType = ""+casDetail.get("api_type");
		
		if (apiType.equals("suma")) casApi = new SumaApi(context, reqRef, casDetail, jsonRequest);
	}

	Map<String,Object> getProductDetail(String productCode) throws Exception {
		
		log.info("CAS DETAIL: "+casDetail);
		
				
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("productAlias", productCode);
		sqlParams.addValue("casId", casDetail.get("cas_id"));				
		
		String sql = " SELECT * FROM cas_product WHERE product_alias=:productAlias AND cas_id=:casId ";
		Map<String,Object> qr = jdt.queryForMap(sql, sqlParams);
		return qr;
	}
	
	void addActivationLog(Map<String,Object> productDetail, Date beginTime, Date endTime, boolean success) {		
		String sysRef = jsonRequestHeader.getString("SysRef");
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("stbId", jsonRequest.getJSONObject("Data").getInt("StbId"));
		sqlParams.addValue("productId", productDetail.get("product_id"));
		sqlParams.addValue("beginTime", beginTime);
		sqlParams.addValue("endTime", endTime);
		sqlParams.addValue("sysRef", sysRef);
		sqlParams.addValue("success", success);
		sqlParams.addValue("activation", (beginTime.after(endTime))?0:1);
		sqlParams.addValue("userId", jsonRequest.getJSONObject("User").getInt("user_id"));
		
		String sql = " INSERT INTO log_product (log_id,user_id,stb_id,product_id,begin_time,end_time,success,activation) VALUES ((SELECT log_id FROM log_req WHERE req_ref=:sysRef),:userId,:stbId,:productId,:beginTime,:endTime,:success,:activation) ";
		jdt.update(sql, sqlParams);
		
	}
	
}
