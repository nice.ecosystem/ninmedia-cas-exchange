package ninmedia.cx.api;

import org.springframework.http.HttpStatus;
import org.json.JSONObject;

public class ApiResponse {
	private JSONObject jsonResult;
	private HttpStatus httpStatus;
	
	private ApiResponse(JSONObject jsonResult, HttpStatus httpStatus) {
		this.jsonResult = jsonResult;
		this.httpStatus = httpStatus;
	}
	
	public static ApiResponse createInstance(JSONObject jsonResult, HttpStatus httpStatus) {
		return new ApiResponse(jsonResult, httpStatus);
	}

	public JSONObject getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(JSONObject jsonResult) {
		this.jsonResult = jsonResult;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
