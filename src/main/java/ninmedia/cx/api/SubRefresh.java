package ninmedia.cx.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import ninmedia.cx.utils.Utils;

public class SubRefresh extends AbstractApi {
	
	private JSONObject subDetail = null;
	long subId = 0;
	
	@Override
	public ApiResponse getResponse() throws Exception {	
		String cardId = null;
		subId = jsonRequestData.getLong("SubId");
		try {
			getSubDetail();
			jsonRequest.getJSONObject("Data").put("QR", subDetail.getString("qr"));
			jsonRequest.getJSONObject("Data").put("ProductId", subDetail.getString("product_alias"));			
			jsonRequestData.put("QR", subDetail.getString("qr"));
			jsonRequestData.put("ProductId", subDetail.getString("product_alias"));			
		} catch (Exception e) {
			log.info("Failed to get sub detail ID: "+subId+" "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2401, "Invalid sub ID: "+subId+"!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
				
		try {
			Utils.validateCardId(jsonRequest);
		} catch (Exception e) {
			log.info("Failed validate Card Id: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2102, "Invalid Card ID or QR!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		cardId = ""+jsonRequestData.getLong("CardId");
				
		try {
			getCasDetail();
		} catch (Exception e) {
			log.info("Failed when getting CAS detail: "+e);
			return ApiResponse.createInstance(Utils.generateJsonError(2101, "Couldn't find detail of CAS: "+casName, jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		Map<String,Object> productDetail = null;
		try {
			productDetail = getProductDetail(jsonRequestData.getString("ProductId"));
		} catch (Exception e) {
			log.info("Failed validate Product Id: "+e,e);
			return ApiResponse.createInstance(Utils.generateJsonError(2103, "Invalid product!", jsonRequest), HttpStatus.BAD_REQUEST);
		}
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date beginTime = df.parse(""+subDetail.get("begin_time"));
		Date endTime = df.parse(""+subDetail.get("end_time"));

		int beginTimeS = (int) (beginTime.getTime()/1000L);
		int endTimeS = (int) (endTime.getTime()/1000L);
		
		ApiResponse apiResp = casApi.authProduct(cardId, productDetail, beginTimeS, endTimeS);															
		
		return apiResp;
	}
	
	private void getSubDetail() throws Exception {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("subId", subId);
		
		String sql = " SELECT r.*,b.chip_id, b.qr,a.cas_id,a.cas_alias, p.product_alias " + 
				" FROM `sub_req` r INNER JOIN stb b ON (b.stb_id=r.stb_id) INNER JOIN cas_product p ON (p.product_id=r.cas_product_id) INNER JOIN cas a ON (a.cas_id=p.cas_id) " + 
				" WHERE 1 AND sub_id=:subId  ";
		
		Map<String,Object> qr = jdt.queryForMap(sql, paramMap);
		
		long stbId = (long) qr.get("stb_id");
		long casProductId = (long) qr.get("cas_product_id");
		
		paramMap.addValue("stbId", stbId);
		paramMap.addValue("casProductId", casProductId);
		
		sql = " SELECT r.*,b.chip_id, b.qr,a.cas_id,a.cas_alias, p.product_alias " + 
				" FROM `sub_req` r INNER JOIN stb b ON (b.stb_id=r.stb_id) INNER JOIN cas_product p ON (p.product_id=r.cas_product_id) INNER JOIN cas a ON (a.cas_id=p.cas_id) " + 
				" WHERE 1 AND r.stb_id=:stbId AND r.cas_product_id=:casProductId AND is_active=1 ORDER BY end_time DESC LIMIT 0,1  ";
		qr = jdt.queryForMap(sql, paramMap);
		
		subDetail = new JSONObject(qr);
	}

}
