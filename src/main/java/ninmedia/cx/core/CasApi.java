package ninmedia.cx.core;

import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Map;

import ninmedia.cx.utils.LogUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import ninmedia.cx.api.ApiResponse;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class CasApi {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	final Map<String, Object> casDetail;
	final String casAlias;
	final int casId;
	final String casHost;
	final int casPort;
	final String reqRef;
	final JSONObject jsonRequest;
	final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	int retryCount = 0;
	final ApplicationContext context;
	final NamedParameterJdbcTemplate jdt;
	final LogUtils logUtils;
	
	public CasApi(ApplicationContext context, String reqRef, Map<String, Object> casDetail, JSONObject jsonRequest) {
		this.casDetail = casDetail;
		this.reqRef = reqRef;
		this.casAlias = ""+casDetail.get("cas_alias");
		this.casId = (int) ((long) casDetail.get("cas_id"));
		this.casHost = ""+casDetail.get("cas_ip");
		this.casPort = (int) ((long) casDetail.get("cas_port"));
		this.jsonRequest = jsonRequest;
		this.context = context;
		this.jdt = (NamedParameterJdbcTemplate) context.getBean("jdt");
		this.logUtils = (LogUtils) context.getBean("logUtils");
		retryCount = 0;
	}
	
	public abstract ApiResponse openAccount(String cardNumber) throws Exception;
	public abstract ApiResponse closeAccount(String cardNumber) throws Exception;
	public abstract ApiResponse sendOsd(String cardNumber, String message) throws Exception;
	public abstract ApiResponse authProduct(String cardNumber, Map<String,Object> productDetail, int beginTimeS, int endTimeS) throws Exception;
	
	void addStbActive(String cardId, int userId) throws Exception {
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("cardId", cardId);
		sqlParams.addValue("userId", userId);
		
		String sql = " INSERT INTO log_stb (stb_id, user_id,active_count) (SELECT stb_id,:userId,1 FROM stb WHERE chip_id=:cardId) ON DUPLICATE KEY UPDATE last_activity=NOW(), active_count=active_count+1 ";
		jdt.update(sql, sqlParams);
	}
	
	
}
