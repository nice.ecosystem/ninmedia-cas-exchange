package ninmedia.cx.core;

import java.util.HashMap;
import java.util.Map;

public class SumaException extends Exception {
	private final String errorCode;
	private final static Map<String,String> errorMap = new HashMap<>();
	
	public SumaException(String errorCode) {			
		super(getErrorMessage(errorCode));
		this.errorCode = errorCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public static String getErrorMessage(String errorCode) {
		String errorMsg = null;
		if (errorMap.size() <= 0) {
			errorMap.put("0xD0000002", "SMS sent an incorrect CAS version. When the value of CAS_Ver in packet header does not equal to CAS version, this error code returns");
			errorMap.put("0xD0000004", "Unsupported command type");
			errorMap.put("0xD0000005", "Data packet length error");
			errorMap.put("0xD000000A", "Terminal ID should not be 0");
			errorMap.put("0xD000001E", "Account of this terminal is illegal");
			errorMap.put("0xFFFFFFFF", "Abnormal communication with CAS (SMS need to re-establish the connection, and send command again)");
			errorMap.put("0xD000000B", "Expire time sent by SMS is incorrect");
			errorMap.put("0xD000001B", "Terminal ID should not be 0xFFFFFFFF");		
		}
		
		errorMsg = (errorMap.get(errorCode)==null)?"Unknown error ("+errorCode+")":errorMap.get(errorCode);
				
		return errorMsg;
	}
}
