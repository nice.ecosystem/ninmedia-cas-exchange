package ninmedia.cx.core;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SumaQueueSender {
	
	public static final String QUEUE_NAME = "SUMA";

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	final Connection connection;
	final Channel channel;	
	private final int casId;	
	
	public static SumaQueueSender createInstance(int casId) {
		SumaQueueSender sender = null;		
		try {			
			sender = new SumaQueueSender(casId);			
		} catch (Exception e) {
			log.error("Error creating sender: "+e,e);
		}		
		return sender;
	}
	
	private SumaQueueSender(int casId) throws IOException, TimeoutException {			
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");	    
		connection = factory.newConnection();
		channel = connection.createChannel();
		this.casId = casId;
	}
	
	
	public String send(byte[] packet, byte[] header, byte[] body) throws IOException, TimeoutException, InterruptedException {
		log.info("New sending message request received!");
		SumaQueueMessage sumaMsg = new SumaQueueMessage(casId, packet, header, body);
	    
		final String corrId = UUID.randomUUID().toString();
		String replyQueueName = channel.queueDeclare().getQueue();
		//log.info("reply queue name: "+replyQueueName);
		
		AMQP.BasicProperties props = new AMQP.BasicProperties
	            .Builder()
	            .correlationId(corrId)
	            .replyTo(replyQueueName)
	            .build();

		channel.basicPublish("", QUEUE_NAME, props, sumaMsg.toString().getBytes());	
		
		final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);
		
		String ctag = channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				if (properties.getCorrelationId().equals(corrId)) {
					response.offer(new String(body));
				}
			}
		});
		
		String result = response.take();
	    channel.basicCancel(ctag);
	    
	    log.info("Received result: "+result);
	    
	    channel.close();
	    connection.close();
	    
	    return result;
	}	
}
