package ninmedia.cx.core;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import ninmedia.cx.utils.SocketUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class SumaQueueReceiver implements ApplicationRunner {
	@Autowired
	SocketUtil socketUtil;

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	final Connection connection;
	final Channel channel;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		try {
			startService();
		} catch (Exception e) {

		}
	}
	
	public SumaQueueReceiver() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");	    
		connection = factory.newConnection();
		channel = connection.createChannel();
	}
	
	public void startService() throws IOException, TimeoutException {
		log.info("Starting SUMA request queue receiver ....");
			  
	    channel.queueDeclare(SumaQueueSender.QUEUE_NAME, false, false, false, null);
	    channel.queuePurge(SumaQueueSender.QUEUE_NAME);
	    channel.basicQos(5);

	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
	    	
	    	AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
	    			.correlationId(properties.getCorrelationId())
	    			.build();	    		    	
	        
	    	SumaQueueMessage sumaMsg;
			try {								
				String rawMessage = new String(body, "UTF-8");
				SumaQueueReceiver.log.info("Raw message: "+rawMessage);
				sumaMsg = SumaQueueMessage.parse(rawMessage);	
				
				// process
				String respText = (new JSONObject().put("success", false).toString());
				try {
					SumaQueueResponse sumaResp = socketUtil.sendRequest(sumaMsg.getCasId(), sumaMsg.getPacket());
					respText = sumaResp.getJson().put("success", true).toString();
				} catch (Exception e) {
					SumaQueueReceiver.log.error("Error sending socket request: "+e,e);
				}				
				
				///HttpClientUtil.post("https://detik.com", "YES");																				
				channel.basicPublish( "", properties.getReplyTo(), replyProps, respText.getBytes("UTF-8"));
				channel.basicAck(envelope.getDeliveryTag(), false);
	            // RabbitMq consumer worker thread notifies the RPC server owner thread 
	            synchronized(this) {
	            	this.notify();
	            }
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error("Error: "+e,e);
			}	    	  	        
	      }
	    };
	    	    
	    channel.basicConsume(SumaQueueSender.QUEUE_NAME, false, consumer);
	    log.info("SUMA request queue receiver started!");	    
	}
}
