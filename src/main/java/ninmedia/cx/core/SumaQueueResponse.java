package ninmedia.cx.core;

import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

public class SumaQueueResponse {
	private final byte[] header;
	private final byte[] body;
	
	private final short respSessionId;
	private final byte respCasVersion;
	private final byte respCommandId;
	private final short respBodyLength;
	private final int respErrorIntCode;
	
	private final JSONObject jsonResp = new JSONObject();
	
	public SumaQueueResponse(byte[] header, byte[] body) throws Exception {
		this.header = header;
		this.body = body;
		
		ByteBuffer byteBufferHeader = ByteBuffer.wrap(header);
		
		respSessionId = byteBufferHeader.getShort();
		respCasVersion = byteBufferHeader.get();
		respCommandId = byteBufferHeader.get();
		respBodyLength = byteBufferHeader.getShort();
		
		ByteBuffer byteBufferBody = ByteBuffer.wrap(body);		
		respErrorIntCode = byteBufferBody.getInt();		
		
		jsonResp.put("errorCode", respErrorIntCode);
		jsonResp.put("respSessionId", respSessionId);
		jsonResp.put("respCasVersion", respCasVersion);
		jsonResp.put("respCommandId", respCommandId);
		jsonResp.put("respBodyLength", respBodyLength);
		jsonResp.put("header", respBodyLength);
		jsonResp.put("body", Base64.encodeBase64URLSafeString(body));
		jsonResp.put("header", Base64.encodeBase64URLSafeString(header));		
	}
	
	public JSONObject getJson() {
		return jsonResp;
	}
	
	public int getErrorCode() {
		return respErrorIntCode;
	}
	
	
}
