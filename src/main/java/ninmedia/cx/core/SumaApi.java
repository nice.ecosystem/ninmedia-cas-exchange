package ninmedia.cx.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import ninmedia.cx.api.ApiResponse;
import ninmedia.cx.utils.ByteBuilder;
import ninmedia.cx.utils.Utils;

public class SumaApi extends CasApi {

	private final int userId;
	private final byte casVersion;
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public SumaApi(ApplicationContext context, String reqRef, Map<String, Object> casDetail, JSONObject jsonRequest) {
		super(context, reqRef, casDetail, jsonRequest);		//
		userId = jsonRequest.getJSONObject("User").getInt("user_id");
		casVersion = Byte.parseByte(Utils.getConfig("suma.cas.version"));
	}

	private final static Map<Integer,Socket> socketPool = new HashMap<>();
	private final static Map<Integer,Timer> socketTimerPool = new HashMap<>();
	private static Timer socketTimer = null;
		
	
	@Override
	public ApiResponse openAccount(String cardNumber)  throws Exception {
		final byte COMMAND_TYPE_ID = 33;
		return openOrCloseAccount(COMMAND_TYPE_ID, cardNumber);
	}
	
	@Override
	public ApiResponse closeAccount(String cardNumber) throws Exception {
		final byte COMMAND_TYPE_ID = 21;
		return openOrCloseAccount(COMMAND_TYPE_ID, cardNumber);
	}
	
	public ApiResponse openOrCloseAccount(final byte COMMAND_TYPE_ID, final String cardNumber) {
		log.info(((COMMAND_TYPE_ID==33)?"OPEN":"CLOSE")+" ACCOUNT, Chip: "+cardNumber+", Command Type: "+COMMAND_TYPE_ID);
		try {
			int cardId = Integer.parseInt(cardNumber);
			switch (COMMAND_TYPE_ID) {
				case 33: log.info("SUMA OPEN ACCOUNT!"); break;
				case 21: log.info("SUMA CLOSE ACCOUNT!"); break;
			}			
			
			// command id for open account
							
			int expTime = getDefaultExpTime();
			
			log.info("Card ID: "+cardId);
																						
			ByteBuilder bodyByteBulder = ByteBuilder.create();
			bodyByteBulder.put(cardId);
			bodyByteBulder.put(expTime);
			bodyByteBulder.put((byte) 1);
									
			short sessionId = (short) getSessionId(jsonRequest);		
			
			sendReceiveData(COMMAND_TYPE_ID, sessionId, bodyByteBulder.getBytes(), jsonRequest);
			
			JSONObject jsonResult = Utils.generateJsonSuccess(jsonRequest);
			jsonResult.getJSONObject("Data").put("SessionId", sessionId);
			jsonResult.getJSONObject("Data").put("CardId", cardNumber);
			
			// add STB active
			addStbActive(cardNumber, userId);
									
			return ApiResponse.createInstance(jsonResult, HttpStatus.OK);
		} catch (SumaException ex) {
			JSONObject jsonError = Utils.generateJsonError(2112, "["+casAlias+"] Unexpected result from CAS!", jsonRequest);
			jsonError.getJSONObject("Data").put("CasErrorCode", ex.getErrorCode());
			jsonError.getJSONObject("Data").put("CasErrorMessage", ex.getMessage());
			return ApiResponse.createInstance(jsonError, HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			log.error("Unknown eror opening account: "+ex,ex);
		}				
		return ApiResponse.createInstance(Utils.generateJsonError(2111, "["+casAlias+"] Unknown error while open account!", jsonRequest), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	public ApiResponse sendOsd(String cardNumber, String message)  throws Exception {
		log.info("SEND OSD Chip: "+cardNumber+", Message: "+message);
		ApiResponse openAccountResponse = activateAccountFirst(cardNumber);
		
		if (!openAccountResponse.getJsonResult().getJSONObject("Header").getBoolean("Success")) return openAccountResponse;	
		
		final byte COMMAND_TYPE_ID = 2;
		final short SHOW_TIME_LEN = 0;
		final byte SHOW_TIMES = Byte.parseByte(Utils.getConfig("suma.osd.showtimes"));
		final byte SHOW_TYPE = Byte.parseByte(Utils.getConfig("suma.osd.showtype"));
		final String MESSAGE = (message.length()>120)?message.substring(0, 120).trim():message.trim();
		final byte MESSAGE_LENGTH = (byte) MESSAGE.length();
		final int EXP_TIME = getDefaultExpTime();
		final int CARD_ID = Integer.parseInt(cardNumber);
		final short sessionId = (short) getSessionId(jsonRequest);	
						
		try {						
			
			ByteBuilder bodyByteBulder = ByteBuilder.create();
			bodyByteBulder.put(CARD_ID);
			bodyByteBulder.put(SHOW_TIME_LEN);
			bodyByteBulder.put(SHOW_TIMES);
			bodyByteBulder.put(SHOW_TYPE);
			bodyByteBulder.put(EXP_TIME);
			bodyByteBulder.put(MESSAGE_LENGTH);
			bodyByteBulder.put(MESSAGE);
										
			sendReceiveData(COMMAND_TYPE_ID, sessionId, bodyByteBulder.getBytes(), jsonRequest);
			
			JSONObject jsonResult = Utils.generateJsonSuccess(jsonRequest);
			jsonResult.getJSONObject("Data").put("SessionId", sessionId);
			jsonResult.getJSONObject("Data").put("CardId", cardNumber);
			return ApiResponse.createInstance(jsonResult, HttpStatus.OK);
		} catch (SumaException ex) {
			JSONObject jsonError = Utils.generateJsonError(2112, "["+casAlias+"] Unexpected result from CAS!", jsonRequest);
			jsonError.getJSONObject("Data").put("CasErrorCode", ex.getErrorCode());
			jsonError.getJSONObject("Data").put("CasErrorMessage", ex.getMessage());
			return ApiResponse.createInstance(jsonError, HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			log.error("Unknown eror sending OSD account: "+ex,ex);
			return ApiResponse.createInstance(Utils.generateJsonError(2111, "["+casAlias+"] Unknown error!", jsonRequest), HttpStatus.INTERNAL_SERVER_ERROR);
		}																	
	}
	
	@Override
	public ApiResponse authProduct(String cardNumber, Map<String, Object> productDetail, final int beginTimeS, final int endTimeS) throws Exception {
		log.info("AUTH PRODUCT Chip: "+cardNumber+", Product Detail: "+productDetail+", begin: "+beginTimeS+", end: "+endTimeS);
		ApiResponse openAccountResponse = activateAccountFirst(cardNumber);
		if (!openAccountResponse.getJsonResult().getJSONObject("Header").getBoolean("Success")) return openAccountResponse;
		
		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String sysRef = jsonRequest.getJSONObject("Header").getString("SysRef");
		
		final int CARD_ID = Integer.parseInt(cardNumber);
		final byte COMMAND_TYPE_ID = 1;
		final short PRODUCT_ID = (short) ((int) productDetail.get("product_code"));
		final byte PRODUCT_AMOUNT = 1;		
		final byte SEND_OR_NOT = 1;
		final byte TAPING_CTRL = 0;			
		final int BEGIN_TIME = beginTimeS;		
		final int END_TIME = endTimeS;
		final byte DESC_LEN = 0;
		
		log.info("Product Activation ["+sysRef+"]");
		log.info("Product ID: "+PRODUCT_ID);
		log.info("Begin time: "+BEGIN_TIME+" ("+df.format(new Date(BEGIN_TIME*1000L))+")");
		log.info("End time: "+END_TIME+" ("+df.format(new Date(END_TIME*1000L))+")");
		
		try {
			final short sessionId = (short) getSessionId(jsonRequest);
			
			ByteBuilder bodyByteBulder = ByteBuilder.create();
			bodyByteBulder.put(CARD_ID);
			bodyByteBulder.put(PRODUCT_AMOUNT);
			bodyByteBulder.put(SEND_OR_NOT);
			bodyByteBulder.put(TAPING_CTRL);
			bodyByteBulder.put(PRODUCT_ID);
			bodyByteBulder.put(BEGIN_TIME);
			bodyByteBulder.put(END_TIME);
			bodyByteBulder.put(DESC_LEN);
			
			sendReceiveData(COMMAND_TYPE_ID, sessionId, bodyByteBulder.getBytes(), jsonRequest);
			JSONObject jsonResult = Utils.generateJsonSuccess(jsonRequest);
			jsonResult.getJSONObject("Data").put("SessionId", sessionId);
			jsonResult.getJSONObject("Data").put("CardId", cardNumber);
			return ApiResponse.createInstance(jsonResult, HttpStatus.OK);
			
		} catch (SumaException ex) {
			JSONObject jsonError = Utils.generateJsonError(2112, "["+casAlias+"] Unexpected result from CAS!", jsonRequest);
			jsonError.getJSONObject("Data").put("CasErrorCode", ex.getErrorCode());
			jsonError.getJSONObject("Data").put("CasErrorMessage", ex.getMessage());
			return ApiResponse.createInstance(jsonError, HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			log.error("Unknown eror auth product: "+ex,ex);
			return ApiResponse.createInstance(Utils.generateJsonError(2111, "["+casAlias+"] Unknown error!", jsonRequest), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private ApiResponse activateAccountFirst(String cardNumber) throws Exception {
		String SysRef0 = jsonRequest.getJSONObject("Header").getString("SysRef");
		String SysRef1 = UUID.randomUUID().toString();
		
		copyLogReq(SysRef0, SysRef1, "open.account");
		
		jsonRequest.getJSONObject("Header").put("SysRef", SysRef1);
		log.info("Card "+cardNumber+" is not active yet!");
		ApiResponse openAccountResponse = openAccount(cardNumber);	
		
		logUtils.updateLogResponse(SysRef1, openAccountResponse.getJsonResult(), openAccountResponse.getHttpStatus(), null);
		
		jsonRequest.getJSONObject("Header").put("SysRef", SysRef0);		
		
		TimeUnit.SECONDS.sleep(3);
		return openAccountResponse;
	}
		

	private Socket getSocketOld() throws IOException {		
		Timer timer = null;
		if (socketTimerPool.get(casId)!=null) {
			socketTimerPool.get(casId).cancel();	
			timer = socketTimerPool.get(casId);
			timer = null;			
			socketTimerPool.put(casId, timer);
		}			
		
		if (socketPool.get(casId) == null || socketPool.get(casId).isClosed()) {			
			log.info("Creating new socket to: "+casHost+", port: "+casPort);
			Socket socket = new Socket();
			socket.setSoTimeout(10000);
	    	socket.connect(new InetSocketAddress(casHost, casPort), 10000);
	    	log.info("Socket to "+casHost+" is connected!");
	    	socketPool.put(casId, socket);
		}					
		
		try {			
			socketTimerPool.put(casId, new Timer());
			socketTimerPool.get(casId).schedule(new TimerTask() {			
				@Override
				public void run() {				
					try {
						if (socketPool.get(casId).isClosed()) return;
						log.info("Socket CAS : "+casId+" is idle, closing socket connection!");
						socketPool.get(casId).close();	
						log.info("Socket CAS : "+casId+" closed!");
					} catch (IOException e) {
						log.error("Error while closing socket!");
					}
				}
			}, Long.parseLong(Utils.getConfig("suma.socket.ttl")));
		} catch (IllegalStateException e) {
			log.error("Error while creating new timer! "+e);
		}
				
		return socketPool.get(casId);
	}
	
	private byte[] buildPacket(final byte commandId, short sessionId, final byte[] byteBody) throws IOException {
		short bodyLength = (short) byteBody.length;
				
		
		log.info("Session ID: "+sessionId);
		log.info("Data body length: "+bodyLength);
		
		ByteBuilder packetBuilder = ByteBuilder.create();
		packetBuilder.put(sessionId);
		packetBuilder.put(casVersion);
		packetBuilder.put(commandId);
		packetBuilder.put((short) byteBody.length);
		packetBuilder.put(byteBody);
		
		log.info("Request packet data: "+getByteString(packetBuilder.getBytes()));
		
		return packetBuilder.getBytes();		
	}
	
	private String getByteString(byte[] packet) {
		String result = "";
		for (int i=0;i<packet.length;i++) {
			result+=String.format("%02X ", packet[i]);			
		}
		return result.trim();
	}
	
	private short generateSessionId() {
		short sessionId = (short) (new RandomDataGenerator().nextLong(1000, 32700));
		return sessionId;
	}
	
	private void sendReceiveData(final byte commandId, short sessionId, final byte[] byteBody, JSONObject jsonRequest) throws SumaException, Exception {
		byte[] packet = buildPacket(commandId,sessionId,byteBody);				
		byte[] header = Arrays.copyOf(packet, packet.length - byteBody.length);				
		byte[] body = byteBody;	
		
		addSumaLog(commandId, sessionId, header, body);
		String result = SumaQueueSender.createInstance(casId).send(packet, header, body);
		JSONObject jsonResult = new JSONObject(result);
		
		if (!jsonResult.getBoolean("success")) {
			throw new Exception("Connection error!");
		}
		
		int errorCode = jsonResult.getInt("errorCode");
		if (errorCode!=0) {
			String errorCodeHex = "0x"+Integer.toHexString(errorCode).toUpperCase();			
			SumaException e = new SumaException(errorCodeHex);			
			throw e;
		}
	}
	
	private void sendReceiveDataOld(final byte commandId, short sessionId, final byte[] byteBody, JSONObject jsonRequest) throws SumaException, Exception {
		byte[] packet = buildPacket(commandId,sessionId,byteBody);				
		byte[] header = Arrays.copyOf(packet, packet.length - byteBody.length);				
		byte[] body = byteBody;	
		
		try {
			
			addSumaLog(commandId, sessionId, header, body);
			
			OutputStream output = getSocketOld().getOutputStream();	
			output.write(packet);
			
			InputStream input = getSocketOld().getInputStream();
			header = new byte[6];
			input.read(header);				
			log.info("Response header data: "+getByteString(header));
			
			ByteBuffer byteBuffer = ByteBuffer.wrap(header);
			
			short respSessionId = byteBuffer.getShort();
			byte respCasVersion = byteBuffer.get();
			byte respCommandId = byteBuffer.get();
			short respBodyLength = byteBuffer.getShort();
			
			log.info("Body length: "+respBodyLength);
			
			body = new byte[respBodyLength];
			input.read(body);	
			log.info("Response body data: "+getByteString(body));			
			byteBuffer = ByteBuffer.wrap(body);
			
			int respErrorIntCode = byteBuffer.getInt();
						
			log.info("Response sessionId: "+sessionId+", casVersion: "+respCasVersion+", commandId: "+commandId+", body length: "+respBodyLength+", error code: "+respErrorIntCode+" (0x"+Integer.toHexString(respErrorIntCode).toUpperCase()+") ");
			updateSumaLog(header, body, respErrorIntCode);
			
			if (respErrorIntCode != 0) {			
				String errorCode = "0x"+Integer.toHexString(respErrorIntCode).toUpperCase();			
				SumaException e = new SumaException(errorCode);			
				throw e;
			}
			
		} catch (java.net.SocketException e) {
			if (retryCount > 3) {
				throw e;
			}
			log.warn("Retrying ... retry count: "+retryCount);
			retryCount++;
			try {
				getSocketOld().close();
			} catch (Exception ex) {
				log.error("Error closing connection: "+e,e);
			}				
			sendReceiveData(commandId, sessionId, byteBody, jsonRequest);	
		}									
	}
	
	private static int getDefaultExpTime() {
		Calendar expCal = Calendar.getInstance();
		//int ttlDays = Integer.parseInt(Utils.getConfig("suma.packet.ttl.days"));
		expCal.add(Calendar.DATE, 1);
		
		int expTime = (int) (expCal.getTimeInMillis()/1000L);		
		log.info("Exp. time: "+dateFormat.format(expCal.getTime()));
		log.info("Exp. Time (Unix): "+expTime);
		
		return expTime;
	}
	
	private void addSumaLog(final byte commandId, short sessionId, final byte[] byteHeader, final byte[] byteBody) throws Exception {
		final String sysRef = jsonRequest.getJSONObject("Header").getString("SysRef");
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("commandId", commandId);
		sqlParams.addValue("sessionId", sessionId);
		sqlParams.addValue("byteBodyLength", byteBody.length);
		sqlParams.addValue("byteHeader", Utils.byteArrayToOctet(byteHeader));
		sqlParams.addValue("byteBody", Utils.byteArrayToOctet(byteBody));
		sqlParams.addValue("sysRef", sysRef);
		sqlParams.addValue("casVersion", casVersion);
		sqlParams.addValue("stbId", jsonRequest.getJSONObject("Data").getInt("StbId"));
		
		String sql = " INSERT INTO log_suma (log_id,stb_id,req_command_type,req_session_id,req_cas,req_body_length,req_header, req_body) ";
		sql+= " VALUES  ((SELECT log_id FROM log_req WHERE req_ref=:sysRef),:stbId,:commandId,:sessionId,:casVersion,:byteBodyLength,:byteHeader, :byteBody) ";
		
		jdt.update(sql, sqlParams);
	}
	
	private void updateSumaLog(byte[] header, byte[] body, int respCode) {
		final String sysRef = jsonRequest.getJSONObject("Header").getString("SysRef");
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("sysRef", sysRef);
		sqlParams.addValue("respHeader", Utils.byteArrayToOctet(header));
		sqlParams.addValue("respBody", Utils.byteArrayToOctet(body));
		sqlParams.addValue("respCode", String.format("0X%08X", respCode));
		String sql = " UPDATE log_suma SET resp_on=NOW(3),resp_cas_header=:respHeader, resp_cas_body=:respBody,resp_cas_code=:respCode WHERE log_id=(SELECT log_id FROM log_req WHERE req_ref=:sysRef)  ";
		jdt.update(sql, sqlParams);
	}
	
	private int getSessionId(JSONObject jsonRequest) {
		final String sysRef = jsonRequest.getJSONObject("Header").getString("SysRef");
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("sysRef", sysRef);
		
		String sql = " SELECT (log_id % 32766)+1 FROM log_req WHERE req_ref=:sysRef ";
		return jdt.queryForObject(sql, sqlParams, Integer.class);
	}

	private void copyLogReq(String oldSysRef, String newSysRef, String newMethod) {
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("oldSysRef", oldSysRef);
		sqlParams.addValue("newSysRef", newSysRef);
		sqlParams.addValue("newMethod", newMethod);
		
		String sql = " INSERT INTO log_req (req_ref,req_from_ip,req_from_host,req_token,req_date_utc,req_method,req_user_ref,req_user_id)  ";
		sql+= " (SELECT :newSysRef,req_from_ip,req_from_host,req_token,req_date_utc,:newMethod,req_user_ref,req_user_id FROM log_req WHERE req_ref=:oldSysRef) ";
		jdt.update(sql, sqlParams);
	}

	
}
