package ninmedia.cx.core;

import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

public class SumaQueueMessage {
	private final String refId;
	
	private byte[] packet;
	private byte[] header;
	private byte[] body;
	
	private final JSONObject jsonMsg;
	private final int casId;
	private boolean valid = false;
	
	public SumaQueueMessage(int casId, String refId, byte[] packet, byte[] header, byte[] body) {		
		this.refId = (refId==null)?UUID.randomUUID().toString():refId;
		
		this.packet = packet;
		this.header = header;
		this.body = body;
		
		this.casId = casId;
		jsonMsg = new JSONObject();		
		jsonMsg.put("refId", this.refId);
		jsonMsg.put("casId", this.casId);
		
		jsonMsg.put("packet", Base64.encodeBase64URLSafeString(packet));
		jsonMsg.put("header", Base64.encodeBase64URLSafeString(header));
		jsonMsg.put("body", Base64.encodeBase64URLSafeString(body));
		
		this.valid = true;
		
	}
	
	public SumaQueueMessage(int casId, byte[] packet, byte[] header, byte[] body) {
		this(casId, null, packet, header, body);
	}
	
	public SumaQueueMessage() {
		this(0, null, null, null, null);
		valid = false;
	}
	
	public byte[] getPacket() {
		return packet;
	}
	
	public byte[] getHeader() {
		return header;
	}
	
	public byte[] getBody() {
		return body;
	}
	
	public int getCasId() {
		return casId;
	}
	
	public String getMessageString() {
		return new String(packet);
	}
	
	public JSONObject getJson() {
		return jsonMsg;
	}
	
	public String getRefId() {
		return refId;
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public static SumaQueueMessage parse(String rawMsg) throws Exception {
		JSONObject jsonRaw = new JSONObject(rawMsg);
		
		byte[] packet = Base64.decodeBase64(jsonRaw.getString("packet"));
		byte[] header = Base64.decodeBase64(jsonRaw.getString("header"));
		byte[] body = Base64.decodeBase64(jsonRaw.getString("body"));
		
		String rawRefId = jsonRaw.getString("refId");
		int casId = jsonRaw.getInt("casId");
		
		SumaQueueMessage sumaMsg = new SumaQueueMessage(casId, rawRefId, packet, header, body);		
		sumaMsg.valid = true;
		
		return sumaMsg;
		
	}
	
	@Override
	public String toString() {
		return jsonMsg.toString();
	}
}
